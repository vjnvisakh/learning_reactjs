"use strict";

var template = React.createElement(
    "div",
    null,
    React.createElement(
        "h1",
        null,
        "Hello"
    ),
    React.createElement(
        "p",
        null,
        "Hello JSX"
    )
);

var appRoot = document.getElementById("app");

ReactDOM.render(template, appRoot);
