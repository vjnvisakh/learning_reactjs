# Learning_ReactJs

A set of projects I did to learn ReactJs

<h3>Introduction_to_React</h3>

1.  Tools needed - VS-Code and NodeJs with npm.
2.  Installing live-server
    a.  npm install -g live-server
    b.  live-server -v (Prints the version number).
3.  live-server public (To serve up the public folder)
4.  React CDN - https://unpkg.com/react@16.0.0/umd/react.development.js
5.  ReactDOM CDN - https://unpkg.com/react-dom@16.0.0/umd/react-dom.development.js
6.  https://babeljs.io/. Babel allows us to use new ES6 features to ES5 that will be supported in all older versions of the browser. Type the JSX and see the ES5 way to do it here.
7.  A preset is a babel plugin. We will use <b>env</b> and <b>react</b>.
8.  <b>Installing babel</b> > npm install -g babel-cli@6.24.1. Check by running babel --help
9. <b>Installing presets</b> > npm install babel-preset-react@6.24.1 babel-preset-env@1.5.2
10. /src/app.js has JSX and public/src/app.js has the normal ES5.
11. babel infile(src/app.js) outfile(--out-file=public/scripts/app.js) --presets=react,env
12. <b>Run babel</b>:  babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch
13. React JSX expressions must have a single root div.






